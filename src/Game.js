import React, { useState } from "react";

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}

const ResetButton = ({ onClick }) => {
  return <button onClick={onClick}>reset?</button>;
};

const Square = ({ onClick, value }) => {
  return (
    <button
      style={{ width: "30px", height: "30px" }}
      className="square"
      onClick={onClick}
    >
      {value}
    </button>
  );
};

const Board = ({ squares, onClick }) => {
  const renderSquare = (i) => {
    // <Function key1={value1} key2={value2} .../>
    return <Square value={squares[i]} onClick={() => onClick(i)} />;
  };

  return (
    <div>
      <div className="board-row">
        {renderSquare(0)}
        {renderSquare(1)}
        {renderSquare(2)}
      </div>
      <div className="board-row">
        {renderSquare(3)}
        {renderSquare(4)}
        {renderSquare(5)}
      </div>
      <div className="board-row">
        {renderSquare(6)}
        {renderSquare(7)}
        {renderSquare(8)}
      </div>
    </div>
  );
};

const Game = () => {
  const initialStates = {
    history: [{ squares: Array(9).fill(null) }],
    xIsNext: true,
  };

  const [history, setHistory] = useState(initialStates.history);
  const [xIsNext, setXIsNext] = useState(initialStates.xIsNext);

  const current = history[history.length - 1];
  const winner = calculateWinner(current.squares);

  const status = winner
    ? `Winner: ${winner}`
    : `Next player: ${xIsNext ? "X" : "O"}`;

  const handleClick = (i) => {
    const squares = current.squares.slice();
    // 按鈕不可以重複點選
    if (calculateWinner(squares) || squares[i]) {
      return;
    }

    // 陣列第i個的值
    squares[i] = xIsNext ? "X" : "O";

    setHistory([...history, { squares }]);
    setXIsNext(!xIsNext);
  };

  return (
    <div className="game">
      <div className="game-board">
        {/* <Board squares={current.squares} onClick={handleClick} /> */}
        <Board
          squares={current.squares}
          onClick={(i) => {
            alert(`message: ${status}`);
            return handleClick(i);
          }}
        />
      </div>
      <div className="game-info">
        <div className="status">{status}</div>
        <ol>{/* TODO */}</ol>
        <ResetButton
          onClick={() => {
            setHistory(initialStates.history);
            setXIsNext(initialStates.xIsNext);
          }}
        ></ResetButton>
      </div>
    </div>
  );
};

export default Game;
